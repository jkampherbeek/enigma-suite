# Enigma Suite

*Current version: 2022.1*

Software for astrological research, containing four applications:

- **Charts**: calculation and interpretation of horoscopes.
- **Counting**: statistical analysis of horoscopic data.
- **Cycles**: calculation of cycles.
- **Calculators**: supporting several astronomical calculations.

Enigma is written in *Free Pascal* and it uses *Lazarus* as its IDE. If you want to use this software you need to install additional components. Please check the developers manual for more information. You will find the developers manual in the folder *docu*. Check for the file *devmanual.md*. In the same folder you will also find a functional description in the user manual: the files *usermanual-en.md* and *usermanual-nl.md* (respectively in English and Dutch).

## Windows only

Enigma is explicitly Windows only

## Bilingual

The program supports English and Dutch. The developers manual is only available in English.

## Free and Open source

Enigma is free and open source. For details check the following files in the root of the application:

- copyright.txt 
- gpl-3.0.txt
- sepl.html

## Facebook group

If new information about the Enigma project is available it will be published in the Facebook group **Enigma: Software for Astrological Research**, link: https://www.facebook.com/groups/246475509388734

## Email list

New releases are announced via a mailing list. You can subscribe by sending a mail with 'subscribe' as subject to enigma@radixpro.org

## Websites

More information about Enigma software is available at the following sites:

- http://radixpro.com/enigma For English speaking users.
- http://radixpro.nl/enigma For Dutch speaking users.
- http://radixpro.org/enigma Technical information for developers.





