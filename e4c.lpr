program e4c;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, UnitDashboard, unitlog, UnitTranslation, UnitState, UnitDomain,
  swissdelphi, UnitAstronCalc, UnitServices, unitcalcmain, unitcalcjdnr;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TFormDashboard, FormDashboard);
  Application.CreateForm(TFormCalcMain, FormCalcMain);
  Application.CreateForm(TFormCalcJdNr, FormCalcJdNr);
  Application.Run;
end.

