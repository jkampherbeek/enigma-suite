{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UnitServices;

{< Services to be used from the frontend. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitAstronCalc, UnitDomain, UnitLog;

type

  { TAstronService }

  TAstronService = class
  strict private
    SeFrontend: TSeFrontend;
    Logger: TLogger;
  public
    constructor Create;
    destructor Destroy; override;
    function GetJdUt(PSimpleDate: TSimpleDate): TJdResponse;
  end;


implementation

{ TAstronService }

constructor TAstronService.Create;
begin
  Logger := TLogger.Create;
  SeFrontend := TSeFrontend.Create;
end;

destructor TAstronService.Destroy;
begin
  SeFrontend.Close;
  // No reason to free Logger or SeFrontend as both are singletons.
  inherited;
end;

function TAstronService.GetJdUt(PSimpleDate: TSimpleDate): TjdResponse;
var
  StatusOk: boolean;
  InfoText: string;
  Response: iResponse;
  JdUt: double;
begin
  StatusOk := True;
  InfoText := '';
  JdUt := 0.0;
  try
    JdUt := SeFrontend.JulDay(PSimpleDate);
  except
    on E: Exception do begin
      StatusOk := False;
      InfoText := E.Message;
      Logger.Log('Error while calculating JD in TAstronService.GetJdUt: ' + E.Message);
    end;
  end;
  Result := TJdResponse.Create(JdUt, StatusOk, InfoText);
end;

end.

