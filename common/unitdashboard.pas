{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UnitDashboard;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, Menus, StdCtrls,
  SysUtils, UnitTranslation, UnitCalcMain;

type

  { TFormDashboard }

  TFormDashboard = class(TForm)
    BtnCalc: TButton;
    BtnCharts: TButton;
    BtnCount: TButton;
    BtnCycles: TButton;
    BtnExit: TButton;
    BtnHelp: TButton;
    ImgCalc: TImage;
    ImgCharts: TImage;
    ImgCount: TImage;
    ImgCycles: TImage;
    LblCalc: TLabel;
    LblCharts: TLabel;
    LblCount: TLabel;
    LblCycles: TLabel;
    LblTitle: TLabel;
    MainMenu: TMainMenu;
    MICommon: TMenuItem;
    MIExit: TMenuItem;
    MILanguage: TMenuItem;
    MIHelp: TMenuItem;
    MILangEn: TMenuItem;
    MILangNl: TMenuItem;
    MIHelpCurrentPage: TMenuItem;
    MIHelpAbout: TMenuItem;
    procedure BtnCalcClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImgCalcClick(Sender: TObject);
    procedure MILangEnClick(Sender: TObject);
    procedure MILangNlClick(Sender: TObject);
    procedure StartCalc;
  private
    Rosetta: TRosetta;
    procedure Populate;
  public

  end;

var
  FormDashboard: TFormDashboard;

implementation

const
  SECTION = 'formdashboard';
  SHARED_SECTION = 'shared';

{$R *.lfm}

{ TFormDashboard }

procedure TFormDashboard.FormCreate(Sender: TObject);
begin
  Rosetta := TRosetta.Create;
end;

procedure TFormDashboard.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormDashboard.BtnCalcClick(Sender: TObject);
begin
  StartCalc;
end;

procedure TFormDashboard.ImgCalcClick(Sender: TObject);
begin
  StartCalc;
end;

procedure TFormDashboard.MILangEnClick(Sender: TObject);
begin
  Rosetta.ChangeLanguage('en');
  FormShow(Sender);
end;

procedure TFormDashboard.MILangNlClick(Sender: TObject);
begin
  Rosetta.ChangeLanguage('nl');
  FormShow(Sender);
end;

procedure TFormDashboard.StartCalc;
begin
  FormCalcMain.ShowModal;
end;

procedure TFormDashboard.Populate;
begin
  with Rosetta do begin
    Caption := GetText(SECTION, 'title');
    LblTitle.Caption := GetTExt(SECTION, 'formtitle');
    LblCharts.Caption := GetText(SECTION, 'lblcharts');
    LblCount.Caption := GetText(SECTION, 'lblcount');
    LblCycles.Caption := GetText(SECTION, 'lblcycles');
    LblCalc.Caption := GetText(SECTION, 'lblcalc');
    BtnCharts.Caption := GetText(SECTION, 'btncharts');
    BtnCount.Caption := GetText(SECTION, 'btncount');
    BtnCycles.Caption := GetText(SECTION, 'btncycles');
    BtnCalc.Caption := GetText(SECTION, 'btncalc');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'btnhelp');
    BtnExit.Caption := GetText(SHARED_SECTION, 'btnexit');
    MICommon.Caption := GetText(SECTION, 'micommon');
    MIExit.Caption := GetText(SECTION, 'miexit');
    MIHelp.Caption := GetText(SECTION, 'mihelp');
    MIHelpAbout.Caption := GetText(SECTION, 'mihelpabout');
    MIHelpCurrentPage.Caption := GetText(SECTION, 'mihelpcurrpage');
    MILangEn.Caption := GetText(SECTION, 'milangen');
    MILangNl.Caption := GetText(SECTION, 'milangnl');
    MILanguage.Caption := GetText(SECTION, 'milanguage');
  end;
end;

end.

