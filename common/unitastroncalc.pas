{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UnitAstronCalc;

{< Astronomical calculations. }

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}

interface

uses
  Classes, swissdelphi, SysUtils, UnitDomain;

type

  { TSeFrontend }

  { Frontend to calculations with the Swiss Ephemeris (SE).
  The SE does not support multiple instances, so it is implemented as a singleton based on the last example at
  http://wiki.freepascal.org/Singleton_Pattern }

  TSeFrontend = class
  private
    constructor Init;
  public
     { Static class to prevent using 'Create' as a constructor.
     The constructor is the private method 'Init' and this is how the singleton is implemented. }
    class function Create: TSeFrontend;
    { Close working memory in SE after performing calculations.}
    procedure Close;
    { Calculate Julian Day Number. Always use Universal time for input as DeltaT is automatically taken care off.}
    function JulDay(PDate:TSimpleDate): Double;

  end;

implementation

var
  SeFrontendInstance: TSeFrontend = nil;

{ TSeFrontend }

constructor TSeFrontend.Init;
begin

  inherited Create;
end;

class function TSeFrontend.Create: TSeFrontend;
begin
  if SeFrontendInstance = nil then begin
    SeFrontendInstance := TSeFrontend.Init;
    swe_set_ephe_path('.\\se');   // required to use the SE Data and for initialization of the SE.
  end;
  Result := SeFrontendInstance;
end;

procedure TSeFrontend.Close;
begin
  swe_close;
end;

function TSeFrontend.JulDay(PDate: TSimpleDate): Double;
var
  GregFlag: integer;
begin
  if (PDate.Gregorian) then GregFlag := 1 else GregFlag := 0;
  Result := swe_julday(PDate.Year, PDate.Month, PDate.Day, PDate.Ut, GregFlag);
end;

end.

