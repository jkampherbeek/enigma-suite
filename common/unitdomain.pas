{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UnitDomain;

{< Domain objects to be used by all components of Enigma. }

{$mode objfpc}{$H+}
{$interfaces corba}

interface

uses
  Classes, SysUtils;

type

  { Interface for objects returning a response from a service. }
  iResponse = interface
    ['{059655DA-F9A8-4A42-A3FA-98C4A3E6DCDD}']
    property StatusOk: boolean;
    property InfoText: string;
  end;

  { TJdResponse }
  { The result of the calculation of a Julian Day number. }
  TJdResponse = class(TInterfacedObject, iResponse)
  private
    FJdUt: double;
    FStatusOk: boolean;
    FInfoText: string;
  public
    constructor Create(PJdUt: double; PStatusOk: boolean; PInfoText: string);
    property StatusOk: boolean read FStatusOk;
    { Julian day, based on ephemeris time. }
    property JdUt: double read FJdUt;
    property InfoText: string read FInfoText;
  end;

  { TSimpleDate }

  TSimpleDate = class
  private
    FYear, FMonth, FDay: integer;
    FUt: double;
    FGregorian: boolean;
  public
    constructor Create(PYear, PMonth, PDay: integer; PUt:double; PGregorian: boolean);
    property Year: integer read FYear;
    property Month: integer read FMonth;
    property Day: integer read FDay;
    property Ut: double read FUt;
    property Gregorian: boolean read FGregorian;
  end;


implementation

{ TSimpleDate }

constructor TSimpleDate.Create(PYear, PMonth, PDay: integer; PUt: double; PGregorian: boolean);
begin
  FYear:= PYear;
  FMonth:= PMonth;
  FDay := PDay;
  FUt := PUt;
  FGregorian:= PGregorian;
end;

{ TJdResponse }

constructor TJdResponse.Create(PJdUt: double; PStatusOk: boolean; PInfoText: string);
begin
  FJdUt := PJdUt;
  FStatusOk := PStatusOk;
  FInfoText := PInfoText;
end;

end.
