{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UnitState;

{< Classes that keep state. Calculated results, settings etc. }

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}

interface

uses
  Classes, SysUtils;

type

  { TCenCon }

  { Central Controller. Keeps track of global data. Implemented as singleton. }
  TCenCon = class
  strict private
    constructor Init;
  public
    { Returns singleton instance of TCenCon. }
    class function Create: TCenCon;
  end;

implementation

var
  CenCon: TCenCon = nil;

{ TCenCon }

constructor TCenCon.Init;
begin
  { TODO : Add initialization }
  inherited Create;
end;

class function TCenCon.Create: TCenCon;
begin
  if CenCon = nil then CenCon := TCenCon.Init;
  Result := CenCon;
end;

end.

