{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit UtTranslation;

{< Test for UnitTranslation (Classes for i18n).
   TRostta is implemented as a singleton and that conflicts with a Teardown method so this method has been omitted. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, UnitTranslation;

type

  { TestRosetta }

  TestRosetta = class(TTestCase)
  protected
    Rosetta: TRosetta;
    procedure SetUp; override;
  published
    procedure TestHappyFlow;
    procedure TestLanguageSwitch;
    procedure TestSectionNotFound;
    procedure TestEntryNotFound;
  end;

implementation

procedure TestRosetta.TestHappyFlow;
begin
  Rosetta.ChangeLanguage('en');
  AssertEquals('Text for entry 1', Rosetta.GetText('test', 'entry1'));
end;

procedure TestRosetta.TestLanguageSwitch;
begin
  Rosetta.ChangeLanguage('nl');
  AssertEquals('Tekst voor entry 2', Rosetta.GetText('test', 'entry2'));
  Rosetta.ChangeLanguage('en');
  AssertEquals('Text for entry 2', Rosetta.GetText('test', 'entry2'));
end;

procedure TestRosetta.TestSectionNotFound;
begin
  AssertEquals('-NOT FOUND-', Rosetta.GetText('wrong.section', 'entry1'));
end;

procedure TestRosetta.TestEntryNotFound;
begin
  AssertEquals('-NOT FOUND-', Rosetta.GetText('test', 'wrong.entry'));
end;

procedure TestRosetta.SetUp;
begin
  Rosetta:= TRosetta.Create;
end;


initialization

  RegisterTest(TestRosetta);
end.

