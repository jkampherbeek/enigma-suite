{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit itservices;

{< Integration tests for services. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, unitservices;

type

  { TTestAstronService }

  TTestAstronService= class(TTestCase)
  protected
    AstronService: TAstronService;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHappyFlow;
  end;

implementation

uses
  UnitDomain;

const
  DELTA = 0.00000001;

{ TTestAstronService }

procedure TTestAstronService.SetUp;
begin
  AstronService:= TAstronService.Create;
  inherited SetUp;
end;

procedure TTestAstronService.TearDown;
begin
  FreeAndNil(AstronService);
  inherited TearDown;
end;


procedure TTestAstronService.TestHappyFlow;
  var
    ExpectedJd: double = 2434406.8173611111111;     // 1953-1-29 UT 7:37, JD value is for UT: it includes Delta T.
    SimpleDate: TSimpleDate;
    Response: TJdResponse;
  begin
    SimpleDate := TSimpleDate.Create(1953, 1, 29, 7.6166666666667, true);
    Response := AstronService.GetJdUt(SimpleDate);
    assertEquals('Calculate Julian Day for UT', ExpectedJd, Response.JdUt, DELTA);
    assertEquals('', Response.InfoText);
    assertTrue(Response.StatusOk);
end;



initialization

  RegisterTest(TTestAstronService);
end.

