{  ---------------------------------------------------------------------------------
  Enigma Suite. A suite of programs for astrological research.
  This software is open source and free to use.
  Please check the file copyright.txt in the root of the source for further details.
  Developed by Jan Kampherbeek, 2022.
  ----------------------------------------------------------------------------------}

unit utdomain;

{< Test for common domainobjects (used in more than one component of Enigma. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry, unitdomain;

type

  { TTestJdResponse }

  TTestJdResponse= class(TTestCase)
  protected
    JdUt: double;
    StatusOk: boolean;
    InfoText: string;
    JdResponse: TJdResponse;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestJd;
    procedure TestStatusOk;
    procedure TestInfoText;
  end;

  { TTestSimpleDate }

  TTestSimpleDate = class(TTestCase)
  protected
    SimpleDate: TSimpleDate;
    Year, Month, Day: integer;
    Ut: double;
    Gregorian: boolean;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestYear;
    procedure TestMonth;
    procedure TestDay;
    procedure TestUt;
    procedure TestGregorian;
  end;

implementation

const
  MARGIN = 0.00000001;

{ TTestSimpleDate }

procedure TTestSimpleDate.SetUp;
begin
  SimpleDate := TSimpleDate.Create(Year, Month, Day, Ut, Gregorian);
  inherited SetUp;
end;

procedure TTestSimpleDate.TearDown;
begin
  FreeAndNil(SimpleDate);
  inherited TearDown;
end;

procedure TTestSimpleDate.TestYear;
begin
  AssertEquals(Year, SimpleDate.Year);
end;

procedure TTestSimpleDate.TestMonth;
begin
  AssertEquals(Month, SimpleDate.Month);
end;

procedure TTestSimpleDate.TestDay;
begin
  AssertEquals(Day, SimpleDate.Day);
end;

procedure TTestSimpleDate.TestUt;
begin
  AssertEquals(Ut, SimpleDate.Ut, MARGIN);
end;

procedure TTestSimpleDate.TestGregorian;
begin
  AssertEquals(Gregorian, SimpleDate.Gregorian);
end;

{ TTestJdResponse }

procedure TTestJdResponse.SetUp;
begin
  JdUt:= 1.2345678901234;
  StatusOk:= true;
  InfoText:= 'Everything OK';
  JdResponse:= TJdResponse.Create(JdUt, StatusOk, InfoText);
end;

procedure TTestJdResponse.TearDown;
begin
  FreeAndNil(JdResponse);
end;

procedure TTestJdResponse.TestJd;
begin
  AssertEquals(JdUt, JdResponse.JdUt, MARGIN);
end;

procedure TTestJdResponse.TestStatusOk;
begin
  AssertEquals(StatusOk, JdResponse.StatusOk);
end;

procedure TTestJdResponse.TestInfoText;
begin
  AssertEquals(InfoText, JdResponse.InfoText);
end;


initialization
  RegisterTest(TTestSimpleDate);
  RegisterTest(TTestJdResponse);
end.

