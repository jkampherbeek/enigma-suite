program e4cUnitTests;

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, GuiTestRunner, unitlog, UtTranslation, UnitTranslation,
  UnitDomain, UnitServices, utdomain, itservices;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

