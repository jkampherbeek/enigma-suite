unit unitcalcjdnr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, UnitTranslation, UnitServices;

type

  { TODO : Add selection for Gregorian Julian calendar }
  { TODO : Add entry for time }
  { TODO : Add validation }
  { TODO : Handle errors in response }

  { TFormCalcJdNr }

  TFormCalcJdNr = class(TForm)
    BtnCalc: TButton;
    BtnExit: TButton;
    BtnHelp: TButton;
    EdDay: TEdit;
    EdMonth: TEdit;
    EdResult: TEdit;
    EdYear: TEdit;
    LblDay: TLabel;
    LblMonth: TLabel;
    LblResult: TLabel;
    LblTitle: TLabel;
    LblYear: TLabel;
    procedure BtnCalcClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Rosetta: TRosetta;
    AstronService: TAstronService;
    procedure Populate;
    procedure Calculate;
  public

  end;

var
  FormCalcJdNr: TFormCalcJdNr;

implementation

uses
  UnitDomain;

const
  SECTION = 'formcalcjdnr';
  SHARED_SECTION = 'shared';

{$R *.lfm}

{ TFormCalcJdNr }

procedure TFormCalcJdNr.FormCreate(Sender: TObject);
begin
  Rosetta := TRosetta.Create;
  AstronService := TAstronService.Create;
end;

procedure TFormCalcJdNr.BtnCalcClick(Sender: TObject);
begin
  Calculate;
end;

procedure TFormCalcJdNr.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormCalcJdNr.Populate;
begin
  with Rosetta do begin
    Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'title');
    LblYear.Caption := GetText(SECTION, 'lblyear');
    LblMonth.Caption := GetText(SECTION, 'lblmonth');
    LblDay.Caption := GetText(SECTION, 'lblday');
    LblResult.Caption := GetText(SECTION, 'lblresult');
    BtnCalc.Caption := GetText(SHARED_SECTION, 'btncalc');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'btnhelp');
    BtnExit.Caption := GetText(SHARED_SECTION, 'btnexit');
  end;
end;

procedure TFormCalcJdNr.Calculate;
var
  year, month, day: integer;
  SimpleDate: TSimpleDate;
  Response: TJdResponse;
begin
  Year := StrToInt(EdYear.Text);
  Month := StrToInt(EdMonth.Text);
  Day := StrToInt(EdDay.Text);
  SimpleDate := TSimpleDate.Create(Year, Month, Day, 0.0, true);
  Response := AstronService.GetJdUt(SimpleDate);
  EdResult.Text := FloatToStr(Response.JdUt);
end;

end.

