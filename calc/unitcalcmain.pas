unit unitcalcmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, UnitTranslation, UnitCalcJdNr;

type

  { TFormCalcMain }

  TFormCalcMain = class(TForm)
    BtnCalcJd: TButton;
    LblTitle: TLabel;
    procedure BtnCalcJdClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Rosetta: TRosetta;
    procedure Populate;
  public

  end;

var
  FormCalcMain: TFormCalcMain;

implementation

const
SECTION = 'formcalcmain';
{$R *.lfm}

{ TFormCalcMain }

procedure TFormCalcMain.FormCreate(Sender: TObject);
begin
  Rosetta:= TRosetta.Create;
end;

procedure TFormCalcMain.BtnCalcJdClick(Sender: TObject);
begin
  FormCalcJdNr.ShowModal;
end;

procedure TFormCalcMain.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormCalcMain.Populate;
begin
  with Rosetta do begin
  Caption := GetText(SECTION, 'formtitle');
  LblTitle.Caption:= GetText(SECTION, 'title');
  end;

end;

end.

